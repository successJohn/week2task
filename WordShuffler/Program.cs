﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace WordShuffler
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a list of words you want to shuffle ");

            string input = Console.ReadLine();

            var result = input.Split(" ").ToArray();
            var randomGenerator = new Random();

            var methodSyntax = result.OrderBy(WordShuffler => randomGenerator.Next());


            Console.WriteLine(" Method syntax for shuffled word");

            Console.WriteLine($"Shuffled list of inputed words: {string.Join(" ",methodSyntax)}");

            Console.WriteLine(" Method syntax for shuffled word");

            var querySyntax = from word in result
                              orderby randomGenerator.Next()
                              select word;


            Console.WriteLine($"Shuffled list of inputed words: {string.Join(" ", methodSyntax)}");
        }
    }
}
