﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections;
using System.Threading.Tasks;

namespace LoginSystem
{



    public delegate void bannedUserAlert();


    public class CBN
    {


        public event bannedUserAlert IsBanned;

        private static List<User> ExistingUsers = new List<User>()
        {
                new User{ Name = "james", AccountNo = "3012343323", BVN = "0124859503939"},
                new User { Name = "jane",  AccountNo = "3024343323", BVN = "0124859503940" },
                new User {Name = "bill" , AccountNo = "3065343354", BVN = "01248595039"},
                new User { Name = "dara", Email = "dara@gmail.com", AccountNo = "0052356780" ,  BVN = "22232348971" },
                new User { Name = "success", Email = "success@gmail.com", AccountNo = "3240678954" , BVN = "57490879673" },
                new User { Name = "mary", Email = "mary@gmail.com", AccountNo = "2327898903" , BVN = "1234576890" },
                new User {Name = "chikki", Email = "chinesechikki@yahoo.com", AccountNo = "4567894334"},
                new User {Name = "uriel", Email = "superUriel@yahoo.com", AccountNo = "4599674334"}

        };


        public  void FindUser(string accountNumber, string name)
        {
            
            var FindUsers = from user in ExistingUsers
                            where user.AccountNo.Equals(accountNumber) && user.Name.Equals(name)
                            select user;

            

                foreach (var user in FindUsers)
            {
             
                 if (IsUserBanned(accountNumber))
                    {
                        Console.Beep();
                        Console.Beep();
                        Console.Beep();
                        Console.Beep();
                        Console.Beep();
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("YOU HAVE BEEN BANNED FROM USING THIS SERVICE !!!");
                        OnIsBanned();
                }else if (user.BVN != null)
                    {
                    Console.WriteLine($" YOUR BVN IS : {user.BVN}");
                }
                

                
            }
           


        }

        

        public static void EnrollUser (string accountNumber, string name)
        {
            var NewBVN = User.GenerateBVN();
            var FindUsers = from user in ExistingUsers
                            where user.AccountNo.Equals(accountNumber) && user.Name.Equals(name)
                            select user;


            
            foreach (var user in FindUsers)
            {


                if (String.IsNullOrWhiteSpace(user.BVN))
                {
                    
                        user.BVN +=  NewBVN;

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"Bvn enrollment Successful");
                        Console.WriteLine($"BVN:  {user.BVN}");
                        Console.ForegroundColor = ConsoleColor.White;
                    

                } else if (user.BVN != null)
                {
                    Console.WriteLine($"You Can't Enroll More than Once , Your BVN IS {user.BVN}");
                }
                else
                {
                    Console.WriteLine("You Are Not A Registered Users");
                }
            }



        }

        private static bool IsUserBanned(string accountNumber)
        {
            accountNumber = accountNumber.Trim();
            if (accountNumber == "3012343323" || accountNumber == "3024343323" || accountNumber == "3065343354") { 
            return true;
            }
            else { return false; }
            
        }

        public void raiseFireAlarm()
        {
            onRaiseAlarm();
        }


        public void BannedUserInfo(object sender, string accNumber)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine($"Banned User {sender} attempted to log in");
            Console.ResetColor();
        }

        protected virtual void onRaiseAlarm()
        {
            //RaiseAlarm?.Invoke();
        }

        protected virtual void OnIsBanned()
        {
            IsBanned?.Invoke();
        }

        /*
        public void InvalidLogin()
        {
        var  bannedUsers = new[] { "Jane", "John", "Ben" };


          
            Console.WriteLine("Click 1 for existing User and 2 for new User");
            var names = Console.ReadLine();
            if (bannedUsers.Contains(names)){

                OnInvalidLogin();

            }
        }
        

        protected virtual void OnInvalidLogin()
        {
           //Console.WriteLine( " An unauthorized user tried to login");

            LogIn?.Invoke();
        }
        */


       
    }
    
}
